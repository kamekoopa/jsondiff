from setuptools import setup, find_packages

version = "0.1"

requires = []

setup(
    name="jsondiff",
    version=version,
    description="jsondiff",
    author="Keisuke Kamada",
    author_email="ewigkeit1204@gmail.com",
    url="https://bitbucket.org/ewigkeit1204/jsondiff",
    packages=find_packages(),
    install_requires=requires,
)

